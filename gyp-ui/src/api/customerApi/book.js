import request from '@/utils/request'

// 查询商品管理列表
export function listShop(query) {
  return request({
    url: '/book/shop/list',
    method: 'get',
    params: query
  })
}
