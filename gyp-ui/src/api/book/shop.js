import request from '@/utils/request'

// 查询商品管理列表
export function listShop(query) {
  return request({
    url: '/book/shop/list',
    method: 'get',
    params: query
  })
}

// 查询商品管理详细
export function getShop(shopId) {
  return request({
    url: '/book/shop/' + shopId,
    method: 'get'
  })
}

// 新增商品管理
export function addShop(data) {
  return request({
    url: '/book/shop',
    method: 'post',
    data: data
  })
}

// 修改商品管理
export function updateShop(data) {
  return request({
    url: '/book/shop',
    method: 'put',
    data: data
  })
}

// 删除商品管理
export function delShop(shopId) {
  return request({
    url: '/book/shop/' + shopId,
    method: 'delete'
  })
}

// 导出商品管理
export function exportShop(query) {
  return request({
    url: '/book/shop/export',
    method: 'get',
    params: query
  })
}