import request from '@/utils/request'

// 查询图书分类列表
export function listCategory(query) {
  return request({
    url: '/book/category/list',
    method: 'get',
    params: query
  })
}
// 查询图书分类列表---树性结构
export function listTreeCategory(query) {
  return request({
    url: '/book/category/list2',
    method: 'get',
    params: query
  })
}
// 查询图书分类详细
export function getCategory(categoryId) {
  return request({
    url: '/book/category/' + categoryId,
    method: 'get'
  })
}

// 新增图书分类
export function addCategory(data) {
  return request({
    url: '/book/category',
    method: 'post',
    data: data
  })
}

// 修改图书分类
export function updateCategory(data) {
  return request({
    url: '/book/category',
    method: 'put',
    data: data
  })
}

// 删除图书分类
export function delCategory(categoryId) {
  return request({
    url: '/book/category/' + categoryId,
    method: 'delete'
  })
}

// 导出图书分类
export function exportCategory(query) {
  return request({
    url: '/book/category/export',
    method: 'get',
    params: query
  })
}
