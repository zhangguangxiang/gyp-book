import request from '@/utils/request'

// 查询订单管理列表
export function listBookorder(query) {
  return request({
    url: '/book/bookorder/list',
    method: 'get',
    params: query
  })
}
// 查询订单管理列表--用于用户查看
export function listBookorder2(query) {
  return request({
    url: '/book/bookorder/list2',
    method: 'get',
    params: query
  })
}
// 查询订单管理详细
export function getBookorder(orderId) {
  return request({
    url: '/book/bookorder/' + orderId,
    method: 'get'
  })
}

// 新增订单管理
export function addBookorder(data) {
  return request({
    url: '/book/bookorder',
    method: 'post',
    data: data
  })
}

// 修改订单管理
export function updateBookorder(data) {
  return request({
    url: '/book/bookorder',
    method: 'put',
    data: data
  })
}

// 删除订单管理
export function delBookorder(orderId) {
  return request({
    url: '/book/bookorder/' + orderId,
    method: 'delete'
  })
}

// 导出订单管理
export function exportBookorder(query) {
  return request({
    url: '/book/bookorder/export',
    method: 'get',
    params: query
  })
}
