import request from '@/utils/request'

// 查询购物车管理列表
export function listShopping(query) {
  return request({
    url: '/book/shopping/list',
    method: 'get',
    params: query
  })
}

// 查询用户购物车管理列表
export function listShopping2(query) {
  return request({
    url: '/book/shopping/list2',
    method: 'get',
    params: query
  })
}
// 查询购物车管理详细
export function getShopping(shopingId) {
  return request({
    url: '/book/shopping/' + shopingId,
    method: 'get'
  })
}

// 新增购物车管理
export function addShopping(data) {
  return request({
    url: '/book/shopping',
    method: 'post',
    data: data
  })
}

// 修改购物车管理
export function updateShopping(data) {
  return request({
    url: '/book/shopping',
    method: 'put',
    data: data
  })
}

// 删除购物车管理
export function delShopping(shopingId) {
  return request({
    url: '/book/shopping/' + shopingId,
    method: 'delete'
  })
}

// 导出购物车管理
export function exportShopping(query) {
  return request({
    url: '/book/shopping/export',
    method: 'get',
    params: query
  })
}
