package com.ruoyi.book.controller;

import java.util.List;

import com.ruoyi.book.domain.ShoppingVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.book.domain.Shopping;
import com.ruoyi.book.service.IShoppingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 购物车管理Controller
 * 
 * @author ganyipeng
 * @date 2021-04-29
 */
@RestController
@RequestMapping("/book/shopping")
public class ShoppingController extends BaseController
{
    @Autowired
    private IShoppingService shoppingService;

    /**
     * 查询购物车管理列表
     */
    @PreAuthorize("@ss.hasPermi('book:shopping:list')")
    @GetMapping("/list")
    public TableDataInfo list(Shopping shopping)
    {
        startPage();
        List<Shopping> list = shoppingService.selectShoppingList(shopping);
        return getDataTable(list);
    }

    /**
     * 查询购物车管理列表
     */
    @GetMapping("/list2")
    public TableDataInfo list2(Shopping shopping)
    {
        startPage();
        List<ShoppingVO> list = shoppingService.selectShoppingVOList(shopping);
        return getDataTable(list);
    }

    /**
     * 导出购物车管理列表
     */
    @PreAuthorize("@ss.hasPermi('book:shopping:export')")
    @Log(title = "购物车管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Shopping shopping)
    {
        List<Shopping> list = shoppingService.selectShoppingList(shopping);
        ExcelUtil<Shopping> util = new ExcelUtil<Shopping>(Shopping.class);
        return util.exportExcel(list, "shopping");
    }

    /**
     * 获取购物车管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('book:shopping:query')")
    @GetMapping(value = "/{shopingId}")
    public AjaxResult getInfo(@PathVariable("shopingId") Long shopingId)
    {
        return AjaxResult.success(shoppingService.selectShoppingById(shopingId));
    }

    /**
     * 新增购物车管理
     */
    @PreAuthorize("@ss.hasPermi('book:shopping:add')")
    @Log(title = "购物车管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Shopping shopping)
    {
        return toAjax(shoppingService.insertShopping(shopping));
    }

    /**
     * 修改购物车管理
     */
    @PreAuthorize("@ss.hasPermi('book:shopping:edit')")
    @Log(title = "购物车管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Shopping shopping)
    {
        return toAjax(shoppingService.updateShopping(shopping));
    }

    /**
     * 删除购物车管理
     */
    @PreAuthorize("@ss.hasPermi('book:shopping:remove')")
    @Log(title = "购物车管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{shopingIds}")
    public AjaxResult remove(@PathVariable Long[] shopingIds)
    {
        return toAjax(shoppingService.deleteShoppingByIds(shopingIds));
    }
}
