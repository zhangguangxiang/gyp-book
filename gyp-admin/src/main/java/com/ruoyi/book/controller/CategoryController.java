package com.ruoyi.book.controller;

import java.util.List;

import com.ruoyi.book.domain.CategoryVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.book.domain.Category;
import com.ruoyi.book.service.ICategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 图书分类Controller
 * 
 * @author ganyipeng
 * @date 2021-05-08
 */
@RestController
@RequestMapping("/book/category")
public class CategoryController extends BaseController
{
    @Autowired
    private ICategoryService categoryService;

    /**
     * 查询图书分类列表
     */
    @PreAuthorize("@ss.hasPermi('book:category:list')")
    @GetMapping("/list")
    public AjaxResult list(Category category)
    {
        List<Category> list = categoryService.selectCategoryList(category);
        return AjaxResult.success(list);
    }

    /**
     * 查询图书分类列表
     */
    @GetMapping("/list2")
    public AjaxResult list2(Category category)
    {
        List<CategoryVO> list = categoryService.selectCategoryVOList(category);
        return AjaxResult.success(list);
    }

    /**
     * 导出图书分类列表
     */
    @PreAuthorize("@ss.hasPermi('book:category:export')")
    @Log(title = "图书分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Category category)
    {
        List<Category> list = categoryService.selectCategoryList(category);
        ExcelUtil<Category> util = new ExcelUtil<Category>(Category.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取图书分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('book:category:query')")
    @GetMapping(value = "/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId)
    {
        return AjaxResult.success(categoryService.selectCategoryById(categoryId));
    }

    /**
     * 新增图书分类
     */
    @PreAuthorize("@ss.hasPermi('book:category:add')")
    @Log(title = "图书分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Category category)
    {
        return toAjax(categoryService.insertCategory(category));
    }

    /**
     * 修改图书分类
     */
    @PreAuthorize("@ss.hasPermi('book:category:edit')")
    @Log(title = "图书分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Category category)
    {
        return toAjax(categoryService.updateCategory(category));
    }

    /**
     * 删除图书分类
     */
    @PreAuthorize("@ss.hasPermi('book:category:remove')")
    @Log(title = "图书分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{categoryIds}")
    public AjaxResult remove(@PathVariable Long[] categoryIds)
    {
        return toAjax(categoryService.deleteCategoryByIds(categoryIds));
    }
}
