package com.ruoyi.book.controller;

import java.util.List;

import com.ruoyi.book.domain.BookOrderVO;
import com.ruoyi.book.domain.BookOrderVO2;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.book.domain.BookOrder;
import com.ruoyi.book.service.IBookOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单管理Controller
 * 
 * @author ganyipeng
 * @date 2021-05-03
 */
@RestController
@RequestMapping("/book/bookorder")
public class BookOrderController extends BaseController
{
    @Autowired
    private IBookOrderService bookOrderService;

    /**
     * 查询订单管理列表
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:list')")
    @GetMapping("/list")
    public TableDataInfo list(BookOrder bookOrder)
    {
        startPage();
        List<BookOrder> list = bookOrderService.selectBookOrderList(bookOrder);
        return getDataTable(list);
    }

    /**
     * 查询订单管理列表
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:list')")
    @GetMapping("/list2")
    public TableDataInfo list2(BookOrder bookOrder)
    {
        startPage();
        List<BookOrderVO2> list = bookOrderService.selectBookOrderList2(bookOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单管理列表
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:export')")
    @Log(title = "订单管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BookOrder bookOrder)
    {
        List<BookOrder> list = bookOrderService.selectBookOrderList(bookOrder);
        ExcelUtil<BookOrder> util = new ExcelUtil<BookOrder>(BookOrder.class);
        return util.exportExcel(list, "bookorder");
    }

    /**
     * 获取订单管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:query')")
    @GetMapping(value = "/{orderId}")
    public AjaxResult getInfo(@PathVariable("orderId") Long orderId)
    {
        return AjaxResult.success(bookOrderService.selectBookOrderById(orderId));
    }

    /**
     * 新增订单管理
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:add')")
    @Log(title = "订单管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BookOrderVO bookOrderVO)
    {
        return toAjax(bookOrderService.insertBookOrder(bookOrderVO));
    }

    /**
     * 修改订单管理
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:edit')")
    @Log(title = "订单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BookOrder bookOrder)
    {
        return toAjax(bookOrderService.updateBookOrder(bookOrder));
    }

    /**
     * 删除订单管理
     */
    @PreAuthorize("@ss.hasPermi('book:bookorder:remove')")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orderIds}")
    public AjaxResult remove(@PathVariable Long[] orderIds)
    {
        return toAjax(bookOrderService.deleteBookOrderByIds(orderIds));
    }
}
