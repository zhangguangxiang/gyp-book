package com.ruoyi.book.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单管理对象 book_order
 * 
 * @author ganyipeng
 * @date 2021-05-03
 */
public class BookOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单主键 */
    private Long orderId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderNumber;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private Long shopIds;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long shopNums;

    /** 订单地址 */
    @Excel(name = "订单地址")
    private Long addressId;

    /** 订单金额 */
    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String orderStatus;

    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setOrderNumber(Long orderNumber) 
    {
        this.orderNumber = orderNumber;
    }

    public Long getOrderNumber() 
    {
        return orderNumber;
    }
    public void setShopIds(Long shopIds) 
    {
        this.shopIds = shopIds;
    }

    public Long getShopIds() 
    {
        return shopIds;
    }
    public void setShopNums(Long shopNums) 
    {
        this.shopNums = shopNums;
    }

    public Long getShopNums() 
    {
        return shopNums;
    }
    public void setAddressId(Long addressId) 
    {
        this.addressId = addressId;
    }

    public Long getAddressId() 
    {
        return addressId;
    }
    public void setOrderPrice(BigDecimal orderPrice) 
    {
        this.orderPrice = orderPrice;
    }

    public BigDecimal getOrderPrice() 
    {
        return orderPrice;
    }
    public void setOrderStatus(String orderStatus) 
    {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus() 
    {
        return orderStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderId", getOrderId())
            .append("orderNumber", getOrderNumber())
            .append("shopIds", getShopIds())
            .append("shopNums", getShopNums())
            .append("addressId", getAddressId())
            .append("orderPrice", getOrderPrice())
            .append("orderStatus", getOrderStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
