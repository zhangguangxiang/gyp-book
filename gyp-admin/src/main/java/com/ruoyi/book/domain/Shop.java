package com.ruoyi.book.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品管理对象 shop
 * 
 * @author ganyipeng
 * @date 2021-04-20
 */
public class Shop extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商品ID */
    private Long shopId;

    /** 类别ID */
    @Excel(name = "类别ID")
    private Long categoryId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String shopName;

    /** 商品级别 */
    @Excel(name = "商品级别")
    private String shopLeval;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private BigDecimal shopPrice;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String shopImg;

    /** 商品简介 */
    @Excel(name = "商品简介")
    private String shopIntroduce;

    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setShopName(String shopName) 
    {
        this.shopName = shopName;
    }

    public String getShopName() 
    {
        return shopName;
    }
    public void setShopLeval(String shopLeval) 
    {
        this.shopLeval = shopLeval;
    }

    public String getShopLeval() 
    {
        return shopLeval;
    }
    public void setShopPrice(BigDecimal shopPrice) 
    {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getShopPrice() 
    {
        return shopPrice;
    }
    public void setShopImg(String shopImg) 
    {
        this.shopImg = shopImg;
    }

    public String getShopImg() 
    {
        return shopImg;
    }
    public void setShopIntroduce(String shopIntroduce) 
    {
        this.shopIntroduce = shopIntroduce;
    }

    public String getShopIntroduce() 
    {
        return shopIntroduce;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shopId", getShopId())
            .append("categoryId", getCategoryId())
            .append("shopName", getShopName())
            .append("shopLeval", getShopLeval())
            .append("shopPrice", getShopPrice())
            .append("shopImg", getShopImg())
            .append("shopIntroduce", getShopIntroduce())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
