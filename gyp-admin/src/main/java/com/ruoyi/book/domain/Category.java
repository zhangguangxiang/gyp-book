package com.ruoyi.book.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 图书分类对象 category
 * 
 * @author ganyipeng
 * @date 2021-05-08
 */
public class Category extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long categoryId;

    /** 类别名称 */
    @Excel(name = "类别名称")
    private String categoryName;

    /** 父级编号 */
    @Excel(name = "父级编号")
    private Long categoryFatherid;

    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setCategoryName(String categoryName) 
    {
        this.categoryName = categoryName;
    }

    public String getCategoryName() 
    {
        return categoryName;
    }
    public void setCategoryFatherid(Long categoryFatherid) 
    {
        this.categoryFatherid = categoryFatherid;
    }

    public Long getCategoryFatherid() 
    {
        return categoryFatherid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("categoryId", getCategoryId())
            .append("categoryName", getCategoryName())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("categoryFatherid", getCategoryFatherid())
            .toString();
    }
}
