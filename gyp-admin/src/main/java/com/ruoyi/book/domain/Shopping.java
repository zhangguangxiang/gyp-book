package com.ruoyi.book.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 购物车管理对象 shopping
 * 
 * @author ganyipeng
 * @date 2021-04-29
 */
public class Shopping extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 购物车ID */
    private Long shopingId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long shopId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long shopNum;

    public void setShopingId(Long shopingId) 
    {
        this.shopingId = shopingId;
    }

    public Long getShopingId() 
    {
        return shopingId;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setShopNum(Long shopNum) 
    {
        this.shopNum = shopNum;
    }

    public Long getShopNum() 
    {
        return shopNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shopingId", getShopingId())
            .append("shopId", getShopId())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("shopNum", getShopNum())
            .toString();
    }
}
