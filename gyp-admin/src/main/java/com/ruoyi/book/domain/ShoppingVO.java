package com.ruoyi.book.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 购物车管理对象 shopping
 * 
 * @author ganyipeng
 * @date 2021-04-29
 */
public class ShoppingVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 购物车ID */
    private Long shopingId;

    /** 商品ID */
    @Excel(name = "商品ID")
    private Long shopId;

    /** 类别ID */
    @Excel(name = "类别ID")
    private Long categoryId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String shopName;

    /** 商品级别 */
    @Excel(name = "商品级别")
    private String shopLeval;

    /** 商品价格 */
    @Excel(name = "商品价格")
    private BigDecimal shopPrice;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String shopImg;

    /** 商品简介 */
    @Excel(name = "商品简介")
    private String shopIntroduce;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private Long shopNum;

    public void setShopingId(Long shopingId) 
    {
        this.shopingId = shopingId;
    }

    public Long getShopingId() 
    {
        return shopingId;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setShopNum(Long shopNum) 
    {
        this.shopNum = shopNum;
    }

    public Long getShopNum() 
    {
        return shopNum;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLeval() {
        return shopLeval;
    }

    public void setShopLeval(String shopLeval) {
        this.shopLeval = shopLeval;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getShopImg() {
        return shopImg;
    }

    public void setShopImg(String shopImg) {
        this.shopImg = shopImg;
    }

    public String getShopIntroduce() {
        return shopIntroduce;
    }

    public void setShopIntroduce(String shopIntroduce) {
        this.shopIntroduce = shopIntroduce;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shopingId", getShopingId())
            .append("shopId", getShopId())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("shopNum", getShopNum())
            .toString();
    }
}
