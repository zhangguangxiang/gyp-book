package com.ruoyi.book.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收货地址对象 address
 * 
 * @author ganyipeng
 * @date 2021-04-20
 */
public class Address extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 地址ID */
    private Long addressId;

    /** 收货人电话 */
    @Excel(name = "收货人电话")
    private String addressPhone;

    /** 收货人姓名 */
    @Excel(name = "收货人姓名")
    private String addressName;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String addressText;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    public void setAddressId(Long addressId) 
    {
        this.addressId = addressId;
    }

    public Long getAddressId() 
    {
        return addressId;
    }
    public void setAddressPhone(String addressPhone) 
    {
        this.addressPhone = addressPhone;
    }

    public String getAddressPhone() 
    {
        return addressPhone;
    }
    public void setAddressName(String addressName) 
    {
        this.addressName = addressName;
    }

    public String getAddressName() 
    {
        return addressName;
    }
    public void setAddressText(String addressText) 
    {
        this.addressText = addressText;
    }

    public String getAddressText() 
    {
        return addressText;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("addressId", getAddressId())
            .append("addressPhone", getAddressPhone())
            .append("addressName", getAddressName())
            .append("addressText", getAddressText())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
