package com.ruoyi.book.mapper;

import java.util.List;
import com.ruoyi.book.domain.BookOrder;

/**
 * 订单管理Mapper接口
 * 
 * @author ganyipeng
 * @date 2021-05-03
 */
public interface BookOrderMapper 
{
    /**
     * 查询订单管理
     * 
     * @param orderId 订单管理ID
     * @return 订单管理
     */
    public BookOrder selectBookOrderById(Long orderId);

    /**
     * 查询订单管理列表
     * 
     * @param bookOrder 订单管理
     * @return 订单管理集合
     */
    public List<BookOrder> selectBookOrderList(BookOrder bookOrder);

    /**
     * 新增订单管理
     * 
     * @param bookOrder 订单管理
     * @return 结果
     */
    public int insertBookOrder(BookOrder bookOrder);

    /**
     * 修改订单管理
     * 
     * @param bookOrder 订单管理
     * @return 结果
     */
    public int updateBookOrder(BookOrder bookOrder);

    /**
     * 删除订单管理
     * 
     * @param orderId 订单管理ID
     * @return 结果
     */
    public int deleteBookOrderById(Long orderId);

    /**
     * 批量删除订单管理
     * 
     * @param orderIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBookOrderByIds(Long[] orderIds);
}
