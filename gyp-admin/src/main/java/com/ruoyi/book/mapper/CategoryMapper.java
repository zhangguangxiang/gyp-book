package com.ruoyi.book.mapper;

import java.util.List;
import com.ruoyi.book.domain.Category;

/**
 * 图书分类Mapper接口
 * 
 * @author ganyipeng
 * @date 2021-05-08
 */
public interface CategoryMapper 
{
    /**
     * 查询图书分类
     * 
     * @param categoryId 图书分类ID
     * @return 图书分类
     */
    public Category selectCategoryById(Long categoryId);

    /**
     * 查询图书分类列表
     * 
     * @param category 图书分类
     * @return 图书分类集合
     */
    public List<Category> selectCategoryList(Category category);

    /**
     * 新增图书分类
     * 
     * @param category 图书分类
     * @return 结果
     */
    public int insertCategory(Category category);

    /**
     * 修改图书分类
     * 
     * @param category 图书分类
     * @return 结果
     */
    public int updateCategory(Category category);

    /**
     * 删除图书分类
     * 
     * @param categoryId 图书分类ID
     * @return 结果
     */
    public int deleteCategoryById(Long categoryId);

    /**
     * 批量删除图书分类
     * 
     * @param categoryIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteCategoryByIds(Long[] categoryIds);
}
