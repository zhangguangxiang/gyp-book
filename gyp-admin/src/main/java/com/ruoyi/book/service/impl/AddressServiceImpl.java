package com.ruoyi.book.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.book.mapper.AddressMapper;
import com.ruoyi.book.domain.Address;
import com.ruoyi.book.service.IAddressService;

/**
 * 收货地址Service业务层处理
 * 
 * @author ganyipeng
 * @date 2021-04-20
 */
@Service
public class AddressServiceImpl implements IAddressService 
{
    @Autowired
    private AddressMapper addressMapper;

    /**
     * 查询收货地址
     * 
     * @param addressId 收货地址ID
     * @return 收货地址
     */
    @Override
    public Address selectAddressById(Long addressId)
    {
        return addressMapper.selectAddressById(addressId);
    }

    /**
     * 查询收货地址列表
     * 
     * @param address 收货地址
     * @return 收货地址
     */
    @Override
    public List<Address> selectAddressList(Address address)
    {
        Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
        address.setUserId(userId);
        return addressMapper.selectAddressList(address);
    }

    /**
     * 新增收货地址
     * 
     * @param address 收货地址
     * @return 结果
     */
    @Override
    public int insertAddress(Address address)
    {
        address.setCreateTime(DateUtils.getNowDate());
        Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
        address.setUserId(userId);
        return addressMapper.insertAddress(address);
    }

    /**
     * 修改收货地址
     * 
     * @param address 收货地址
     * @return 结果
     */
    @Override
    public int updateAddress(Address address)
    {
        address.setUpdateTime(DateUtils.getNowDate());
        return addressMapper.updateAddress(address);
    }

    /**
     * 批量删除收货地址
     * 
     * @param addressIds 需要删除的收货地址ID
     * @return 结果
     */
    @Override
    public int deleteAddressByIds(Long[] addressIds)
    {
        return addressMapper.deleteAddressByIds(addressIds);
    }

    /**
     * 删除收货地址信息
     * 
     * @param addressId 收货地址ID
     * @return 结果
     */
    @Override
    public int deleteAddressById(Long addressId)
    {
        return addressMapper.deleteAddressById(addressId);
    }
}
