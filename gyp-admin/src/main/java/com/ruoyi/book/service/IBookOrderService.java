package com.ruoyi.book.service;

import java.util.List;
import com.ruoyi.book.domain.BookOrder;
import com.ruoyi.book.domain.BookOrderVO;
import com.ruoyi.book.domain.BookOrderVO2;

/**
 * 订单管理Service接口
 * 
 * @author ganyipeng
 * @date 2021-05-03
 */
public interface IBookOrderService 
{
    /**
     * 查询订单管理
     * 
     * @param orderId 订单管理ID
     * @return 订单管理
     */
    public BookOrder selectBookOrderById(Long orderId);

    /**
     * 查询订单管理列表
     * 
     * @param bookOrder 订单管理
     * @return 订单管理集合
     */
    public List<BookOrder> selectBookOrderList(BookOrder bookOrder);

    /**
     * 查询订单管理列表
     *
     * @param bookOrder 订单管理
     * @return 订单管理集合
     */
    public List<BookOrderVO2> selectBookOrderList2(BookOrder bookOrder);

    /**
     * 新增订单管理
     * 
     * @param bookOrderVO 订单管理
     * @return 结果
     */
    public int insertBookOrder(BookOrderVO bookOrderVO);

    /**
     * 修改订单管理
     * 
     * @param bookOrder 订单管理
     * @return 结果
     */
    public int updateBookOrder(BookOrder bookOrder);

    /**
     * 批量删除订单管理
     * 
     * @param orderIds 需要删除的订单管理ID
     * @return 结果
     */
    public int deleteBookOrderByIds(Long[] orderIds);

    /**
     * 删除订单管理信息
     * 
     * @param orderId 订单管理ID
     * @return 结果
     */
    public int deleteBookOrderById(Long orderId);
}
