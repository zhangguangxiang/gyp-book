package com.ruoyi.book.service;

import java.util.List;
import com.ruoyi.book.domain.Category;
import com.ruoyi.book.domain.CategoryVO;

/**
 * 图书分类Service接口
 * 
 * @author ganyipeng
 * @date 2021-05-08
 */
public interface ICategoryService 
{
    /**
     * 查询图书分类
     * 
     * @param categoryId 图书分类ID
     * @return 图书分类
     */
    public Category selectCategoryById(Long categoryId);

    /**
     * 查询图书分类列表
     * 
     * @param category 图书分类
     * @return 图书分类集合
     */
    public List<Category> selectCategoryList(Category category);

    public List<CategoryVO> selectCategoryVOList(Category category);

    /**
     * 新增图书分类
     * 
     * @param category 图书分类
     * @return 结果
     */
    public int insertCategory(Category category);

    /**
     * 修改图书分类
     * 
     * @param category 图书分类
     * @return 结果
     */
    public int updateCategory(Category category);

    /**
     * 批量删除图书分类
     * 
     * @param categoryIds 需要删除的图书分类ID
     * @return 结果
     */
    public int deleteCategoryByIds(Long[] categoryIds);

    /**
     * 删除图书分类信息
     * 
     * @param categoryId 图书分类ID
     * @return 结果
     */
    public int deleteCategoryById(Long categoryId);
}
