package com.ruoyi.book.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.ruoyi.book.domain.*;
import com.ruoyi.book.mapper.AddressMapper;
import com.ruoyi.book.mapper.ShopMapper;
import com.ruoyi.book.mapper.ShoppingMapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.book.mapper.BookOrderMapper;
import com.ruoyi.book.service.IBookOrderService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 订单管理Service业务层处理
 * 
 * @author ganyipeng
 * @date 2021-05-03
 */
@Service
public class BookOrderServiceImpl implements IBookOrderService 
{
    @Autowired
    private BookOrderMapper bookOrderMapper;
    @Autowired
    private ShopMapper shopMapper;
    @Autowired
    private ShoppingMapper shoppingMapper;
    @Autowired
    private AddressMapper addressMapper;

    /**
     * 查询订单管理
     * 
     * @param orderId 订单管理ID
     * @return 订单管理
     */
    @Override
    public BookOrder selectBookOrderById(Long orderId)
    {
        return bookOrderMapper.selectBookOrderById(orderId);
    }

    @Override
    public List<BookOrder> selectBookOrderList(BookOrder bookOrder) {
        return bookOrderMapper.selectBookOrderList(bookOrder);
    }

    /**
     * 查询订单管理列表
     * 
     * @param bookOrder 订单管理
     * @return 订单管理
     */
    @Override
    public List<BookOrderVO2> selectBookOrderList2(BookOrder bookOrder)
    {
        Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
        Address address = new Address();
        address.setUserId(userId);
        List<Address> addressList = addressMapper.selectAddressList(address);

        List<BookOrder> bookOrders = new ArrayList<>();

        for (Address address1 : addressList) {
            bookOrder.setAddressId(address1.getAddressId());
            List<BookOrder> bookOrders1 = bookOrderMapper.selectBookOrderList(bookOrder);
            bookOrders.addAll(bookOrders1);
        }
        List<BookOrderVO2> bookOrderVO2s = new ArrayList<>(bookOrders.size());
        for (BookOrder order : bookOrders) {
            BookOrderVO2 bookOrderVO2 = new BookOrderVO2();
            BeanUtils.copyProperties(order,bookOrderVO2);
            Shop shop = shopMapper.selectShopById(order.getShopIds());
            BeanUtils.copyProperties(shop,bookOrderVO2);
            bookOrderVO2s.add(bookOrderVO2);
        }
        return bookOrderVO2s;
    }

    /**
     * 新增订单管理
     * 
     * @param bookOrderVO 订单管理
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBookOrder(BookOrderVO bookOrderVO)
    {
        Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
        Long addressId = bookOrderVO.getAddressId();
        if (addressId==null||addressId==0){
            throw new RuntimeException("订单地址不能为空");
        }
        String[] shopIds = bookOrderVO.getShopIds().split(",");
        String[] shopNums = bookOrderVO.getShopNums().split(",");

        //订单号
        int random = (int)((Math.random()*9+1)*100000);

        for (int i = 0; i < shopIds.length; i++) {
            //删除购物车商品
            Shopping shopping = new Shopping();
            shopping.setShopId(Long.parseLong(shopIds[i]));
            shopping.setUserId(userId);
            List<Shopping> shoppings = shoppingMapper.selectShoppingList(shopping);
            for (Shopping shopping1 : shoppings) {
                shoppingMapper.deleteShoppingById(shopping1.getShopingId());
            }

            BookOrder bookOrder = new BookOrder();
            BeanUtils.copyProperties(bookOrderVO,bookOrder);
            bookOrder.setShopIds(Long.parseLong(shopIds[i]));
            bookOrder.setShopNums(Long.parseLong(shopNums[i]));
            bookOrder.setOrderNumber(Long.parseLong(random+""));

            Shop shop = shopMapper.selectShopById(bookOrder.getShopIds());
            bookOrder.setOrderPrice(shop.getShopPrice().multiply(BigDecimal.valueOf(bookOrder.getShopNums())));
            bookOrder.setOrderStatus("已付款");

            bookOrder.setCreateTime(DateUtils.getNowDate());
            bookOrderMapper.insertBookOrder(bookOrder);
        }
        return 1;
    }

    /**
     * 修改订单管理
     * 
     * @param bookOrder 订单管理
     * @return 结果
     */
    @Override
    public int updateBookOrder(BookOrder bookOrder)
    {
        bookOrder.setUpdateTime(DateUtils.getNowDate());
        return bookOrderMapper.updateBookOrder(bookOrder);
    }

    /**
     * 批量删除订单管理
     * 
     * @param orderIds 需要删除的订单管理ID
     * @return 结果
     */
    @Override
    public int deleteBookOrderByIds(Long[] orderIds)
    {
        return bookOrderMapper.deleteBookOrderByIds(orderIds);
    }

    /**
     * 删除订单管理信息
     * 
     * @param orderId 订单管理ID
     * @return 结果
     */
    @Override
    public int deleteBookOrderById(Long orderId)
    {
        BookOrder bookOrder = bookOrderMapper.selectBookOrderById(orderId);
        BookOrder selectBookOrder = new BookOrder();
        selectBookOrder.setOrderNumber(bookOrder.getOrderNumber());
        List<BookOrder> bookOrders = bookOrderMapper.selectBookOrderList(selectBookOrder);
        for (BookOrder order : bookOrders) {
            bookOrderMapper.deleteBookOrderById(order.getOrderId());
        }
        return 1;
    }
}
