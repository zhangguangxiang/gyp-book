package com.ruoyi.book.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.book.mapper.ShopMapper;
import com.ruoyi.book.domain.Shop;
import com.ruoyi.book.service.IShopService;

/**
 * 商品管理Service业务层处理
 * 
 * @author ganyipeng
 * @date 2021-04-20
 */
@Service
public class ShopServiceImpl implements IShopService 
{
    @Autowired
    private ShopMapper shopMapper;

    /**
     * 查询商品管理
     * 
     * @param shopId 商品管理ID
     * @return 商品管理
     */
    @Override
    public Shop selectShopById(Long shopId)
    {
        return shopMapper.selectShopById(shopId);
    }

    /**
     * 查询商品管理列表
     * 
     * @param shop 商品管理
     * @return 商品管理
     */
    @Override
    public List<Shop> selectShopList(Shop shop)
    {
        return shopMapper.selectShopList(shop);
    }

    /**
     * 新增商品管理
     * 
     * @param shop 商品管理
     * @return 结果
     */
    @Override
    public int insertShop(Shop shop)
    {
        shop.setCreateTime(DateUtils.getNowDate());
        return shopMapper.insertShop(shop);
    }

    /**
     * 修改商品管理
     * 
     * @param shop 商品管理
     * @return 结果
     */
    @Override
    public int updateShop(Shop shop)
    {
        shop.setUpdateTime(DateUtils.getNowDate());
        return shopMapper.updateShop(shop);
    }

    /**
     * 批量删除商品管理
     * 
     * @param shopIds 需要删除的商品管理ID
     * @return 结果
     */
    @Override
    public int deleteShopByIds(Long[] shopIds)
    {
        return shopMapper.deleteShopByIds(shopIds);
    }

    /**
     * 删除商品管理信息
     * 
     * @param shopId 商品管理ID
     * @return 结果
     */
    @Override
    public int deleteShopById(Long shopId)
    {
        return shopMapper.deleteShopById(shopId);
    }
}
