package com.ruoyi.book.service;

import java.util.List;
import com.ruoyi.book.domain.Shopping;
import com.ruoyi.book.domain.ShoppingVO;

/**
 * 购物车管理Service接口
 * 
 * @author ganyipeng
 * @date 2021-04-29
 */
public interface IShoppingService 
{
    /**
     * 查询购物车管理
     * 
     * @param shopingId 购物车管理ID
     * @return 购物车管理
     */
    public Shopping selectShoppingById(Long shopingId);

    /**
     * 查询购物车管理列表
     * 
     * @param shopping 购物车管理
     * @return 购物车管理集合
     */
    public List<Shopping> selectShoppingList(Shopping shopping);

    /**
     * 查询查询购物车管理列表
     *
     * @param shopping 购物车管理
     * @return 购物车管理集合
     */
    public List<ShoppingVO> selectShoppingVOList(Shopping shopping);

    /**
     * 新增购物车管理
     * 
     * @param shopping 购物车管理
     * @return 结果
     */
    public int insertShopping(Shopping shopping);

    /**
     * 修改购物车管理
     * 
     * @param shopping 购物车管理
     * @return 结果
     */
    public int updateShopping(Shopping shopping);

    /**
     * 批量删除购物车管理
     * 
     * @param shopingIds 需要删除的购物车管理ID
     * @return 结果
     */
    public int deleteShoppingByIds(Long[] shopingIds);

    /**
     * 删除购物车管理信息
     * 
     * @param shopingId 购物车管理ID
     * @return 结果
     */
    public int deleteShoppingById(Long shopingId);
}
