package com.ruoyi.book.service;

import java.util.List;
import com.ruoyi.book.domain.Shop;

/**
 * 商品管理Service接口
 * 
 * @author ganyipeng
 * @date 2021-04-20
 */
public interface IShopService 
{
    /**
     * 查询商品管理
     * 
     * @param shopId 商品管理ID
     * @return 商品管理
     */
    public Shop selectShopById(Long shopId);

    /**
     * 查询商品管理列表
     * 
     * @param shop 商品管理
     * @return 商品管理集合
     */
    public List<Shop> selectShopList(Shop shop);

    /**
     * 新增商品管理
     * 
     * @param shop 商品管理
     * @return 结果
     */
    public int insertShop(Shop shop);

    /**
     * 修改商品管理
     * 
     * @param shop 商品管理
     * @return 结果
     */
    public int updateShop(Shop shop);

    /**
     * 批量删除商品管理
     * 
     * @param shopIds 需要删除的商品管理ID
     * @return 结果
     */
    public int deleteShopByIds(Long[] shopIds);

    /**
     * 删除商品管理信息
     * 
     * @param shopId 商品管理ID
     * @return 结果
     */
    public int deleteShopById(Long shopId);
}
