package com.ruoyi.book.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.book.domain.Shop;
import com.ruoyi.book.domain.ShoppingVO;
import com.ruoyi.book.mapper.ShopMapper;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.book.mapper.ShoppingMapper;
import com.ruoyi.book.domain.Shopping;
import com.ruoyi.book.service.IShoppingService;

/**
 * 购物车管理Service业务层处理
 * 
 * @author ganyipeng
 * @date 2021-04-29
 */
@Service
public class ShoppingServiceImpl implements IShoppingService 
{
    @Autowired
    private ShoppingMapper shoppingMapper;
    @Autowired
    private ShopMapper shopMapper;

    /**
     * 查询购物车管理
     * 
     * @param shopingId 购物车管理ID
     * @return 购物车管理
     */
    @Override
    public Shopping selectShoppingById(Long shopingId)
    {
        return shoppingMapper.selectShoppingById(shopingId);
    }

    /**
     * 查询购物车管理列表
     * 
     * @param shopping 购物车管理
     * @return 购物车管理
     */
    @Override
    public List<Shopping> selectShoppingList(Shopping shopping)
    {
        return shoppingMapper.selectShoppingList(shopping);
    }

    /**
     * 前台查询购物车管理列表
     *
     * @param shopping 购物车管理
     * @return 购物车管理
     */
    @Override
    public List<ShoppingVO> selectShoppingVOList(Shopping shopping)
    {
        Long userId = SecurityUtils.getLoginUser().getUser().getUserId();
        shopping.setUserId(userId);
        List<Shopping> shoppings = shoppingMapper.selectShoppingList(shopping);
        List<ShoppingVO> shoppingVOList = new ArrayList<>();
        for (Shopping shopping1 : shoppings) {
            ShoppingVO shoppingVO = new ShoppingVO();
            BeanUtils.copyProperties(shopping1,shoppingVO);
            Shop shop = shopMapper.selectShopById(shopping1.getShopId());
            BeanUtils.copyProperties(shop,shoppingVO);
            shoppingVOList.add(shoppingVO);
        }
        return shoppingVOList;
    }

    /**
     * 新增购物车管理
     * 
     * @param shopping 购物车管理
     * @return 结果
     */
    @Override
    public int insertShopping(Shopping shopping)
    {
        shopping.setCreateTime(DateUtils.getNowDate());
        LoginUser loginUser = SecurityUtils.getLoginUser();
        SysUser user = loginUser.getUser();
        Long userId = user.getUserId();
        shopping.setUserId(userId);

        Shopping shoppingIsExist = new Shopping();
        shoppingIsExist.setShopId(shopping.getShopId());
        shoppingIsExist.setUserId(shopping.getUserId());
        List<Shopping> shoppings = shoppingMapper.selectShoppingList(shoppingIsExist);
        int size = shoppings.size();
        if (size>0){
            Shopping shopping1 = shoppings.get(0);
            shopping1.setShopNum(shopping1.getShopNum()+1);
            return shoppingMapper.updateShopping(shopping1);
        }else {
            return shoppingMapper.insertShopping(shopping);
        }
    }

    /**
     * 修改购物车管理
     * 
     * @param shopping 购物车管理
     * @return 结果
     */
    @Override
    public int updateShopping(Shopping shopping)
    {
        shopping.setUpdateTime(DateUtils.getNowDate());
        return shoppingMapper.updateShopping(shopping);
    }

    /**
     * 批量删除购物车管理
     * 
     * @param shopingIds 需要删除的购物车管理ID
     * @return 结果
     */
    @Override
    public int deleteShoppingByIds(Long[] shopingIds)
    {
        return shoppingMapper.deleteShoppingByIds(shopingIds);
    }

    /**
     * 删除购物车管理信息
     * 
     * @param shopingId 购物车管理ID
     * @return 结果
     */
    @Override
    public int deleteShoppingById(Long shopingId)
    {
        return shoppingMapper.deleteShoppingById(shopingId);
    }
}
