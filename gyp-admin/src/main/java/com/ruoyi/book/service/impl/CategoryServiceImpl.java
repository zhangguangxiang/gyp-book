package com.ruoyi.book.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.book.domain.CategoryVO;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.book.mapper.CategoryMapper;
import com.ruoyi.book.domain.Category;
import com.ruoyi.book.service.ICategoryService;

/**
 * 图书分类Service业务层处理
 * 
 * @author ganyipeng
 * @date 2021-05-08
 */
@Service
public class CategoryServiceImpl implements ICategoryService 
{
    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 查询图书分类
     * 
     * @param categoryId 图书分类ID
     * @return 图书分类
     */
    @Override
    public Category selectCategoryById(Long categoryId)
    {
        return categoryMapper.selectCategoryById(categoryId);
    }

    /**
     * 查询图书分类列表
     * 
     * @param category 图书分类
     * @return 图书分类
     */
    @Override
    public List<Category> selectCategoryList(Category category)
    {
        return categoryMapper.selectCategoryList(category);
    }

    @Override
    public List<CategoryVO> selectCategoryVOList(Category category) {
        List<CategoryVO> categoryVOList = new ArrayList<>();
        category.setCategoryFatherid(0L);
        List<Category> categories = categoryMapper.selectCategoryList(category);
        for (Category category1 : categories) {
            CategoryVO categoryVO = new CategoryVO();
            BeanUtils.copyProperties(category1,categoryVO);
            Category selectCategory = new Category();
            selectCategory.setCategoryFatherid(category1.getCategoryId());
            List<Category> categoriesList = categoryMapper.selectCategoryList(selectCategory);
            categoryVO.setCategoryList(categoriesList);
            categoryVOList.add(categoryVO);
        }
        return categoryVOList;
    }

    /**
     * 新增图书分类
     * 
     * @param category 图书分类
     * @return 结果
     */
    @Override
    public int insertCategory(Category category)
    {
        category.setCreateTime(DateUtils.getNowDate());
        return categoryMapper.insertCategory(category);
    }

    /**
     * 修改图书分类
     * 
     * @param category 图书分类
     * @return 结果
     */
    @Override
    public int updateCategory(Category category)
    {
        category.setUpdateTime(DateUtils.getNowDate());
        return categoryMapper.updateCategory(category);
    }

    /**
     * 批量删除图书分类
     * 
     * @param categoryIds 需要删除的图书分类ID
     * @return 结果
     */
    @Override
    public int deleteCategoryByIds(Long[] categoryIds)
    {
        return categoryMapper.deleteCategoryByIds(categoryIds);
    }

    /**
     * 删除图书分类信息
     * 
     * @param categoryId 图书分类ID
     * @return 结果
     */
    @Override
    public int deleteCategoryById(Long categoryId)
    {
        return categoryMapper.deleteCategoryById(categoryId);
    }
}
