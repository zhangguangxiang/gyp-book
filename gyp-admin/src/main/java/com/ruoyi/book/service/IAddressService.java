package com.ruoyi.book.service;

import java.util.List;
import com.ruoyi.book.domain.Address;

/**
 * 收货地址Service接口
 * 
 * @author ganyipeng
 * @date 2021-04-20
 */
public interface IAddressService 
{
    /**
     * 查询收货地址
     * 
     * @param addressId 收货地址ID
     * @return 收货地址
     */
    public Address selectAddressById(Long addressId);

    /**
     * 查询收货地址列表
     * 
     * @param address 收货地址
     * @return 收货地址集合
     */
    public List<Address> selectAddressList(Address address);

    /**
     * 新增收货地址
     * 
     * @param address 收货地址
     * @return 结果
     */
    public int insertAddress(Address address);

    /**
     * 修改收货地址
     * 
     * @param address 收货地址
     * @return 结果
     */
    public int updateAddress(Address address);

    /**
     * 批量删除收货地址
     * 
     * @param addressIds 需要删除的收货地址ID
     * @return 结果
     */
    public int deleteAddressByIds(Long[] addressIds);

    /**
     * 删除收货地址信息
     * 
     * @param addressId 收货地址ID
     * @return 结果
     */
    public int deleteAddressById(Long addressId);
}
